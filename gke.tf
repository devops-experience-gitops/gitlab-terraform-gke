provider "google" {
  project     = "group-cs-9b54eb"
}

resource "google_container_cluster" "primary" {
  name                     = "hazevedo-devops-experience"
  location                 = "us-central1"
  remove_default_node_pool = false
  initial_node_count       = 1
  min_master_version       = "1.20"
  description              = var.cluster_description
}

resource "google_container_node_pool" "primary_preemptible_nodes" {
  name       = "${var.cluster_name}-node-pool"
  cluster    = google_container_cluster.primary.name
  location   = var.gcp_region
  node_count = 1

  node_config {
    preemptible  = true
    machine_type = var.machine_type

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}
